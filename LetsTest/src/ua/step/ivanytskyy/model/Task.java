package ua.step.ivanytskyy.model;
import java.util.ArrayList;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementWrapper;
/** 
 * Class Task.
 * Class Task was developed for getting and saving list of Answer objects,
 * xml attributes and xml elements values by JAXB.
 * @version 1.14 2015.07.23
 * @author Vitaliy Ivanytskyy
 */
public class Task {
	// The value of the id xml attribute of the task (positive integer number)
	private int id;
	// The value of the question xml element of the task
	private String question;
	// The value of the answersType xml element of the task (two possible values: "multiple" and "single")
	private String answersType;
	// The answers list of the current Task object
	private ArrayList<Answer> answers;
	public int getId() {
		return id;
	}
	@XmlAttribute
	public void setId(int id) {
		this.id = id;
	}
	public String getQuestion() {
		return question;
	}
	@XmlElement
	public void setQuestion(String question) {
		this.question = question.trim();
	}
	public String getAnswersType() {
		return answersType;
	}
	@XmlAttribute
	public void setAnswersType(String answersType) {
		this.answersType = answersType;
	}
	public ArrayList<Answer> getAnswers() {
		return answers;
	}
	@XmlElementWrapper(name = "answers")
	@XmlElement(name="answer", type=Answer.class)
	public void setAnswers(ArrayList<Answer> answers) {
		this.answers = answers;
	}
	@Override
	public String toString() {
		return "Task [id=" + id + ", question=" + question + ", answersType="
				+ answersType + ", answers=" + answers + "]";
	}	
}