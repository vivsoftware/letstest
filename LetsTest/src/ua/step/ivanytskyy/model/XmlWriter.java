package ua.step.ivanytskyy.model;
import java.io.File;
import java.io.IOException;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.Unmarshaller;
/** 
 * Class XmlWriter.
 * The Class XmlWriter was developed for writing data to the xml file by using the JAXB technology.
 * In addition, this class can add new data to old data have already been in the xml file.
 * @version 1.14 2015.07.23
 * @author Vitaliy Ivanytskyy
 */
public class XmlWriter {
	// Id for the first user 
	private final int MIN_ID = 1;
	// Results of testing
	private Result result;
	// The file represent xml file for writing the results of the testing.
	private File file;
	/**
     * Constructs a Result object with the Student object and by using the JAXB technology.
     * @param  newStudent
     *        This object contents the current user information and the results of his testing.
     */ 
	public XmlWriter(Student newStudent) throws IOException {
		file = new File("Results\\results.xml");
		result = new Result();
		if(file.exists()){
			readOldData();
			// Definition id of the previous user
			int lastIndex = result.getStudents().size() - 1;
			int lastId = result.getStudents().get(lastIndex).getId();
			// Set id for the new user
			newStudent.setId(++lastId);	        
		}else{
			file.createNewFile();
			// Set start id for the first user
			newStudent.setId(MIN_ID);
		}		
		result.getStudents().add(newStudent);        
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(
					new Class[]{Result.class, Student.class});
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.marshal(result, file);
		 } catch (JAXBException e) {
			e.printStackTrace();
		 }
	}
	// It is the method for reading old data from the xml file by using JAXB
	private void readOldData(){
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(
					new Class[]{Result.class, Student.class});
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			result = (Result) jaxbUnmarshaller.unmarshal(file);
		 } catch (JAXBException e) {
			e.printStackTrace();
		 }
	}
}