package ua.step.ivanytskyy.model;
import java.util.ArrayList;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementWrapper;
/** 
 * Class Test.
 * Class Test was developed for getting and saving list of Task objects,
 * xml attributes and xml elements values by JAXB.
 * @version 1.14 2015.07.23
 * @author Vitaliy Ivanytskyy
 */
public class Test {
	// The value of the id xml attribute of the test (positive integer number)
	private int id;
	// The value of the name xml element of the test (name of test)
	private String name;
	// The value of the numberOfTasks xml element of the test (positive integer number).
	// This value is predetermined number of tasks for current test
	private int numberOfTasks;
	// The value of the testTime xml element of the test (positive integer number).
	// This value is predetermined duration (in minutes) for current test
	private int testTime;
	// The tasks list of the current Test object
	private ArrayList<Task> tasks;
	public int getId() {
		return id;
	}
	@XmlAttribute
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	@XmlElement
	public void setName(String name) {
		this.name = name;
	}
	public int getNumberOfTasks() {
		return numberOfTasks;
	}
	@XmlElement
	public void setNumberOfTasks(int numberOfTasks) {
		this.numberOfTasks = numberOfTasks;
	}
	public int getTestTime() {
		return testTime;
	}
	@XmlElement
	public void setTestTime(int testTime) {
		this.testTime = testTime;
	}
	public ArrayList<Task> getTasks() {
		return tasks;
	}
	@XmlElementWrapper(name = "tasks")
	@XmlElement(name="task",type=Task.class)
	public void setTasks(ArrayList<Task> tasks) {
		this.tasks = tasks;
	}
	@Override
	public String toString() {
		return "Test [id=" + id + ", name=" + name + ", numberOfTasks="
				+ numberOfTasks + ", testTime=" + testTime + ", tasks=" + tasks
				+ "]";
	}	
}