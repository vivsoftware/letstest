package ua.step.ivanytskyy.model;
import java.io.File;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Unmarshaller;
/** 
 * Class XmlReader.
 * The Class XmlReader was developed for reading data from the xml file by using the JAXB technology.
 * @version 1.14 2015.07.23
 * @author Vitaliy Ivanytskyy
 */
public class XmlReader {
	private Theme theme;
	/**
     * Constructs a Theme object with the File object and by using the JAXB technology.
     * @param  file
     *        This file represent xml file contents xml data for testing.
     */ 
	public XmlReader(File file) {
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(
					new Class[]{Theme.class, Test.class, Task.class, Answer.class});
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			theme = (Theme) jaxbUnmarshaller.unmarshal(file);
		 } catch (JAXBException e) {
			e.printStackTrace();
		 }
	}
	public Theme getTheme() {
		return theme;
	}	
}