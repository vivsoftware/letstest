package ua.step.ivanytskyy.model;
import java.io.File;
import java.util.ArrayList;
/** 
 * Class XmlFileSearcher.
 * The Class XmlFileSearcher was developed for searching the xml files with the specified path.
 * @version 1.14 2015.07.23
 * @author Vitaliy Ivanytskyy
 */
public class XmlFileSearcher {
	// Array with files which was found with the specified path
	private File[] filesArray;
	// Array with names of files which was found with the specified path
	private String[] fileNamesArray;
	public XmlFileSearcher(String path) {
		File file = new File(path);
		File[] filesAndDirectoriesArray = file.listFiles();		
		ArrayList<File> fileListAsArrayList = new ArrayList<File>();
		// Finding xml files in the loop and saving into ArrayList
		for (int i = 0; i < filesAndDirectoriesArray.length; i++) {
			if(filesAndDirectoriesArray[i].isFile() 
					&& filesAndDirectoriesArray[i].getName().endsWith(".xml")){
				fileListAsArrayList.add(filesAndDirectoriesArray[i]);
			}
		}
		// Converting the ArrayList to array
		filesArray = fileListAsArrayList.toArray(new File[fileListAsArrayList.size()]);
		// Getting the file names without extension and saving them in the array.
		// These names will be shown in combobox of CentralPanelOfStartForm object as list of themes
		fileNamesArray = new String[fileListAsArrayList.size()];
		for (int i = 0; i < filesArray.length; i++) {
			int index = filesArray[i].getName().lastIndexOf('.');
			fileNamesArray[i] = filesArray[i].getName().substring(0, index);
		}
	}
	public String[] getFileNamesArray() {
		return fileNamesArray;
	}
	public File[] getFilesArray() {
		return filesArray;
	}	
}