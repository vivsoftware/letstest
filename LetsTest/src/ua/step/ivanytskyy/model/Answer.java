package ua.step.ivanytskyy.model;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
/** 
 * Class Answer.
 * Class Answer was developed for getting and saving xml attributes and xml elements values by JAXB.
 * @version 1.14 2015.07.23
 * @author Vitaliy Ivanytskyy
 */
public class Answer {
	// The value of the id xml attribute of the answer (positive integer number)
	private int id;
	// The value of the mark xml attribute of the answer (two possible values: "correct" and "wrong")
	private String mark;
	// The value of the text xml element of the answer
	private String text;
	public int getId() {
		return id;
	}
	@XmlAttribute
	public void setId(int id) {
		this.id = id;
	}	
	public String getMark() {
		return mark;
	}
	@XmlAttribute
	public void setMark(String mark) {
		this.mark = mark;
	}
	public String getText() {
		return text;
	}
	@XmlElement
	public void setText(String text) {
		this.text = text;
	}
	@Override
	public String toString() {
		return "Answer [id=" + id + ", mark=" + mark + ", text=" + text
				+ "]";
	}	
}