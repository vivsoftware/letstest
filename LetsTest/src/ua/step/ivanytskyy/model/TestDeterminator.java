package ua.step.ivanytskyy.model;
import java.io.File;
import java.util.ArrayList;
/** 
 * Class TestDeterminator.
 * The Class TestDeterminator was developed for creating the tests names array
 * and determination the Test object by using a test name.
 * @version 1.14 2015.07.23
 * @author Vitaliy Ivanytskyy
 */
public class TestDeterminator {
	private ArrayList<Test> testsList;
	private String[] testsNameArray;
	private Test currentTestObj;
	/**
     * Constructs an tests names list with the file object.
     * @param  file
     *        This file represent xml file which contents tests.
     */ 
	public TestDeterminator(File file) {
		XmlReader xmlReader = new XmlReader(file);
		testsList =xmlReader.getTheme().getTests();
		testsNameArray = new String[testsList.size()];
		for (int i = 0; i < testsList.size(); i++) {
			testsNameArray[i] = testsList.get(i).getName();
		}		
	}
	/**
     * Constructs an tests names list with the file object
     * and determination the Test object by using a test name.
     * @param  file
     *        This file represent xml file which contents tests.
     * @param  testName
     *        This testName is name of test which was chosen by an user.
     */ 
	public TestDeterminator(File file, String testName) {
		XmlReader xmlReader = new XmlReader(file);
		testsList =xmlReader.getTheme().getTests();
		testsNameArray = new String[testsList.size()];
		for (int i = 0; i < testsList.size(); i++) {
			if(testsList.get(i).getName().equals(testName)){
				currentTestObj = testsList.get(i);
			}
			testsNameArray[i] = testsList.get(i).getName();
		}		
	}	
	public String[] getTestsNameArray() {
		return testsNameArray;
	}
	public Test getCurrentTestObj() {
		return currentTestObj;
	}	
}