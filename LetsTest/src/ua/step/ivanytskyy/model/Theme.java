package ua.step.ivanytskyy.model;
import java.util.ArrayList;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementWrapper;
import jakarta.xml.bind.annotation.XmlRootElement;
/** 
 * Class Theme.
 * Class Theme was developed for getting and saving list of Test objects and xml elements values by JAXB.
 * @version 1.14 2015.07.23
 * @author Vitaliy Ivanytskyy
 */
@XmlRootElement(name="theme")
public class Theme {
	// The value of the name xml element of the theme (name of theme)
	private String name;
	// The tests list of the current Theme object
	private ArrayList<Test> tests;
	public String getName() {
		return name;
	}
	@XmlElement
	public void setName(String name) {
		this.name = name;
	}
	public ArrayList<Test> getTests() {
		return tests;
	}
	@XmlElementWrapper(name = "tests")
	@XmlElement(name="test",type=Test.class)
	public void setTests(ArrayList<Test> tests) {
		this.tests = tests;
	}
	@Override
	public String toString() {
		return "Theme [name=" + name + ", tests=" + tests + "]";
	}	
}