package ua.step.ivanytskyy.model;
import java.util.ArrayList;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementWrapper;
import jakarta.xml.bind.annotation.XmlRootElement;
/** 
 * Class Result
 * Class Result was developed as storage of xml attributes and xml elements values for all the students (users).
 * This class is required for the JAXB working.
 * @version 1.14 2015.07.23
 * @author Vitaliy Ivanytskyy
 */
@XmlRootElement(name="result")
public class Result {
	// The users list
	private ArrayList<Student> students;
	public Result(){
		students = new ArrayList<Student>();
	}
	@XmlElementWrapper(name = "students")
	@XmlElement(name="student",type=Student.class)
	public ArrayList<Student> getStudents() {
		return students;
	}
	public void setStudents(ArrayList<Student> students) {
		this.students = students;
	}
}