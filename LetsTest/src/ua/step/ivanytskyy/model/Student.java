package ua.step.ivanytskyy.model;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
/** 
 * Class Student
 * Class Student was developed as storage of xml attributes and xml elements values for current user.
 * This class is required for the JAXB working.
 * @version 1.14 2015.07.23
 * @author Vitaliy Ivanytskyy
 */
public class Student {
	// The value for the id xml attribute of the student xml element (positive integer number)
	private int id;
	// The value of the name xml element (the name of the user)
	private String name;
	// The value of the email xml element (the e-mail of the user)	
	private String email;
	// The value of the group xml element (the group number of the user)	
	private String group;
	// The value of the theme xml element (the theme was chosen by the user)
	private String theme;
	// The value of the test xml element (the test was chosen by the user)
	private String test;
	// The value of the date xml element (the date of testing)
	private String date;
	// The value of the numberOfQuestions xml element (the number of questions which was asked for user)
	private int numberOfQuestions;
	// The value of the correctAnswersNumber xml element (the number of correct answers)
	private int correctAnswersNumber;
	// The value of the wrongAnswersNumber xml element (the number of wrong answers)
	private int wrongAnswersNumber;
	// The value of the missedQuestionsNumber xml element (the number of missed answers)
	private int missedQuestionsNumber;
	// The value of the testTime xml element (the duration of testing in fact)
	private String testTime;
	// The value of the grade xml element (value as ratio the correct answers number
	// to the tasks total number of current test (%))
	private int grade;
	@XmlAttribute
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	@XmlElement
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@XmlElement
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	@XmlElement
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	@XmlElement
	public String getTheme() {
		return theme;
	}
	public void setTheme(String theme) {
		this.theme = theme;
	}
	@XmlElement
	public String getTest() {
		return test;
	}
	public void setTest(String test) {
		this.test = test;
	}
	@XmlElement
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	@XmlElement
	public int getNumberOfQuestions() {
		return numberOfQuestions;
	}
	public void setNumberOfQuestions(int numberOfQuestions) {
		this.numberOfQuestions = numberOfQuestions;
	}
	@XmlElement
	public int getCorrectAnswersNumber() {
		return correctAnswersNumber;
	}
	public void setCorrectAnswersNumber(int correctAnswersNumber) {
		this.correctAnswersNumber = correctAnswersNumber;
	}
	@XmlElement
	public int getWrongAnswersNumber() {
		return wrongAnswersNumber;
	}
	public void setWrongAnswersNumber(int wrongAnswersNumber) {
		this.wrongAnswersNumber = wrongAnswersNumber;
	}
	@XmlElement
	public int getMissedQuestionsNumber() {
		return missedQuestionsNumber;
	}
	public void setMissedQuestionsNumber(int missedQuestionsNumber) {
		this.missedQuestionsNumber = missedQuestionsNumber;
	}
	@XmlElement
	public String getTestTime() {
		return testTime;
	}
	public void setTestTime(String testTime) {
		this.testTime = testTime;
	}
	@XmlElement
	public int getGrade() {
		return grade;
	}
	public void setGrade(int grade) {
		this.grade = grade;
	}	
}