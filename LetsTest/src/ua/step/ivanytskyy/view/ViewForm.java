package ua.step.ivanytskyy.view;
import java.awt.BorderLayout;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import ua.step.ivanytskyy.controller.*;
/** 
 * Class ViewForm
 * The class ViewForm is central class in ua.step.ivanytskyy.view package of Let's Test program.
 * This class was developed for three panels (header, central and footer) creating 
 * and integrate them in the main frame.
 * This approach allow dynamically renew content of the frame.  
 * @version 1.14 2015.07.23
 * @author Vitaliy Ivanytskyy
 */
public class ViewForm extends JFrame{
	private static final long serialVersionUID = 8343065352587960381L;
	private JPanel contentPane;
	private HeaderPanel headerPanel;
	private CentralPanel centralPanel;
	private FooterPanel footerPanel;
	private Listener listener;
	public ViewForm(String title,
			HeaderPanel headerPanel,
			CentralPanel centralPanel,
			FooterPanel footerPanel,
			int width, int height) {
		listener = new Listener(this);
		createViewForm(title, headerPanel, centralPanel, footerPanel, width, height);		
	}	
	public final void createViewForm(
			String title,
			HeaderPanel headerPanel,
			CentralPanel centralPanel,
			FooterPanel footerPanel,
			int width, int height){		
		setTitle("Программа тестирования знаний Let's Test");
		setSize(width, height);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// Creating the contentPane
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));		
		// Creating the headerPanel
		this.headerPanel = headerPanel;
		contentPane.add(this.headerPanel.getHeaderPanel(), BorderLayout.NORTH);		
		// Creating the centralPanel and connecting the listener object
		this.centralPanel = centralPanel;
		listener.listenCentralPanel(this.centralPanel);
		contentPane.add(this.centralPanel.getCentralPanel(), BorderLayout.CENTER);		
		// Creating the footerPanel and connecting the listener object
		this.footerPanel = footerPanel;
		listener.listenFooterPanel(this.footerPanel);
		contentPane.add(this.footerPanel.getFooterPanel(), BorderLayout.SOUTH);
		revalidate();
	}		
}