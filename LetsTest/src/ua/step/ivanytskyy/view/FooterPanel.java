package ua.step.ivanytskyy.view;
import javax.swing.*;
/** 
 * Abstract class FooterPanel.
 * The abstract class FooterPanel defines the common interface for his subclasses.
 * @version 1.14 2015.07.23
 * @author Vitaliy Ivanytskyy
 */
public abstract class FooterPanel extends JPanel{
	private static final long serialVersionUID = 8728958708223612740L;
	public abstract JPanel getFooterPanel();
}