package ua.step.ivanytskyy.view;
import java.awt.BorderLayout;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;
/** 
 * Class HeaderPanelOfFinishForm.
 * The HeaderPanelOfFinishForm defines the view of the finish form top part.
 * @version 1.14 2015.07.23
 * @author Vitaliy Ivanytskyy
 */
public class HeaderPanelOfFinishForm extends HeaderPanel{
	private static final long serialVersionUID = 7804749618591909358L;
	private JPanel headerPanel;
	private String messageString = "Прохождение теста завершено!";
	public HeaderPanelOfFinishForm() {
		headerPanel = new JPanel();
		headerPanel.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createBevelBorder(BevelBorder.RAISED),
                BorderFactory.createEmptyBorder(5, 5, 5, 5)));
		JLabel headerPanelText = new JLabel(messageString);
		headerPanel.add(headerPanelText, BorderLayout.CENTER);		
	}
	@Override
	public JPanel getHeaderPanel() {
		return headerPanel;
	}	
}