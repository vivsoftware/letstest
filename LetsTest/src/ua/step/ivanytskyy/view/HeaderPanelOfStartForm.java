package ua.step.ivanytskyy.view;
import java.awt.BorderLayout;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;
/** 
 * Class HeaderPanelOfStartForm.
 * The HeaderPanelOfStartForm defines the view of the start form top part.
 * @version 1.14 2015.07.23
 * @author Vitaliy Ivanytskyy
 */
public class HeaderPanelOfStartForm extends HeaderPanel{
	private static final long serialVersionUID = -1025335456110588198L;	
	private JPanel headerPanel;
	private String instructionString = "До начала тестирования введите исходные данные:";
	public HeaderPanelOfStartForm() {
		headerPanel = new JPanel();
		headerPanel.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createBevelBorder(BevelBorder.RAISED),
                BorderFactory.createEmptyBorder(5, 5, 5, 5)));
		JLabel headerPanelText = new JLabel(instructionString);
		headerPanel.add(headerPanelText, BorderLayout.CENTER);		
	}
	@Override
	public JPanel getHeaderPanel() {
		return headerPanel;
	}	
}