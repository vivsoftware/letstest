package ua.step.ivanytskyy.view;
import java.awt.Dimension;
import javax.swing.JButton;
import javax.swing.JPanel;
/** 
 * Class FooterPanelOfTaskForm.
 * The FooterPanelOfTaskForm defines the view of the task form bottom part.
 * @version 1.14 2015.07.23
 * @author Vitaliy Ivanytskyy
 */
public class FooterPanelOfTaskForm extends FooterPanel{
	private static final long serialVersionUID = 2215513179910967438L;
	private JPanel footerPanel;
	private JButton buttonPostpone;
	private JButton buttonNext;
	private String buttonPostponeName = "Отложить";
	private String buttonNextName = "Следующий";
	public FooterPanelOfTaskForm() {
		footerPanel = new JPanel();
		Dimension preferredSize = new Dimension(160, 26);
		buttonPostpone = new JButton(buttonPostponeName);
		buttonPostpone.setPreferredSize(preferredSize);
		buttonNext = new JButton(buttonNextName);
		buttonNext.setPreferredSize(preferredSize);
		footerPanel.add(buttonPostpone);
		footerPanel.add(buttonNext);
	}
	@Override
	public JPanel getFooterPanel() {
		return footerPanel;
	}
	public JButton getButtonPostpone() {
		return buttonPostpone;
	}
	public JButton getButtonNext() {
		return buttonNext;
	}
}