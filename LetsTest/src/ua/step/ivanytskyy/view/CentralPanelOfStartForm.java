package ua.step.ivanytskyy.view;
import java.awt.Font;
import java.awt.GridLayout;
import javax.swing.*;
import javax.swing.border.BevelBorder;
import ua.step.ivanytskyy.model.*;
/** 
 * Class CentralPanelOfStartForm.
 * The CentralPanelOfStartForm defines the view of the start form central part.
 * @version 1.14 2015.07.23
 * @author Vitaliy Ivanytskyy
 */
public class CentralPanelOfStartForm extends CentralPanel{
	private static final long serialVersionUID = 596321034180723828L;
	private JPanel centralPanel;
	private JTextField nameTextField;
	private JTextField emailTextField;
	private JTextField groupTextField;
	private JComboBox<String> themesComboBox;
	// It's label for showing the message about correctness of entered user name
	private JLabel nameTextFieldChecker;
	// It's label for showing the message about correctness of entered user email
	private JLabel eMailTextFieldChecker;
	// It's label for showing the message about correctness of entered user group
	private JLabel groupTextFieldChecker;
	// It's label for showing the message if theme was selected by the user
	private JLabel themesComboBoxChecker;
	// Getting the list of the available themes
	private String[] themesList = new XmlFileSearcher("Tests\\").getFileNamesArray();
	private JComboBox<String> testsComboBox;
	// This label use for correct work of GridLayout (like a cap)
	private JLabel emptyLabel;
	private JLabel tasksNumberLabel;
	private JLabel testTimeLabel;
	public CentralPanelOfStartForm() {
		centralPanel = new JPanel();
		centralPanel.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createBevelBorder(BevelBorder.LOWERED),
                BorderFactory.createEmptyBorder(25, 25, 25, 25)));		
		centralPanel.setLayout(new GridLayout(7, 3, 20, 5));
		Font fontForValueLabel = new Font(Font.DIALOG, Font.ITALIC, 12);
		// Creating the block for receiving the user name
		JLabel titleNameTextField = new JLabel("ФИО");
		centralPanel.add(titleNameTextField);
		nameTextField = new JTextField();
		centralPanel.add(nameTextField);
		nameTextField.setColumns(10);		
		nameTextFieldChecker = new JLabel("пусто");
		nameTextFieldChecker.setFont(fontForValueLabel);
		centralPanel.add(nameTextFieldChecker);
		// Creating the block for receiving the user email
		JLabel titleEMailTextField = new JLabel("E-Mail");
		centralPanel.add(titleEMailTextField);
		emailTextField = new JTextField();
		centralPanel.add(emailTextField);
		emailTextField.setColumns(10);
		eMailTextFieldChecker = new JLabel("пусто");
		eMailTextFieldChecker.setFont(fontForValueLabel);
		centralPanel.add(eMailTextFieldChecker);
		// Creating the block for receiving the user group name
		JLabel titleGroupTextField = new JLabel("Группа");
		centralPanel.add(titleGroupTextField);
		groupTextField = new JTextField();
		centralPanel.add(groupTextField);
		emailTextField.setColumns(10);
		groupTextFieldChecker = new JLabel("пусто");
		groupTextFieldChecker.setFont(fontForValueLabel);
		centralPanel.add(groupTextFieldChecker);
		// Creating the block for receiving the theme name which was chosen by the user
		JLabel titleThemesComboBox = new JLabel("Тема");
		centralPanel.add(titleThemesComboBox);
		themesComboBox = new JComboBox<>(themesList);		
		centralPanel.add(themesComboBox);
		themesComboBoxChecker = new JLabel("не выбрана");
		themesComboBoxChecker.setFont(fontForValueLabel);
		centralPanel.add(themesComboBoxChecker);
		// Creating the block for receiving the test name which was chosen by the user
		JLabel titleListOfTestsComboBox = new JLabel("Список тестов");
		centralPanel.add(titleListOfTestsComboBox);
		testsComboBox = new JComboBox<>();
		centralPanel.add(testsComboBox);
		emptyLabel = new JLabel();
		centralPanel.add(emptyLabel);
		// Creating the block for showing the number of questions into the test which was chosen by the user
		JLabel titleNumberOfTasksLabel = new JLabel("Количество вопросов:");
		centralPanel.add(titleNumberOfTasksLabel);
		tasksNumberLabel = new JLabel();
		centralPanel.add(tasksNumberLabel);
		emptyLabel = new JLabel();
		centralPanel.add(emptyLabel);
		// Creating the block for showing the total duration of the test which was chosen by the user
		JLabel titleTestTimeLabel = new JLabel("Время прохождения:");
		centralPanel.add(titleTestTimeLabel);
		testTimeLabel = new JLabel();
		centralPanel.add(testTimeLabel);
		emptyLabel = new JLabel();
		centralPanel.add(emptyLabel);		
	}
	@Override
	public JPanel getCentralPanel() {
		return centralPanel;
	}	
	public JTextField getNameTextField() {
		return nameTextField;
	}
	public JTextField getEmailTextField() {
		return emailTextField;
	}
	public JTextField getGroupTextField() {
		return groupTextField;
	}
	public JLabel getNameTextFieldChecker() {
		return nameTextFieldChecker;
	}
	public JLabel getEMailTextFieldChecker() {
		return eMailTextFieldChecker;
	}
	public JLabel getGroupTextFieldChecker() {
		return groupTextFieldChecker;
	}
	public JLabel getThemesComboBoxChecker() {
		return themesComboBoxChecker;
	}
	public void setTestsList(String[] testsList) {
		testsComboBox.removeAllItems();
		for (String test : testsList){
			testsComboBox.addItem(test);
		}
	}
	public JComboBox<String> getThemesComboBox() {
		return themesComboBox;
	}
	public JComboBox<String> getTestsComboBox() {
		return testsComboBox;
	}	
	public void setThemesList(String[] themesList) {
		testsComboBox.removeAll();
		themesComboBox.removeAllItems();
		for(String theme : themesList){
			themesComboBox.addItem(theme);
		}
	}
	public void setTasksNumber(String tasksNumber) {
		this.tasksNumberLabel.setText(tasksNumber);
	}
	public void setTestTime(String testTime) {
		this.testTimeLabel.setText(testTime);
	}
}