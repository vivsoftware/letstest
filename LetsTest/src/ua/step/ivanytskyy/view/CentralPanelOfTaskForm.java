package ua.step.ivanytskyy.view;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.BevelBorder;
import ua.step.ivanytskyy.model.Answer;
import ua.step.ivanytskyy.model.Task;
/** 
 * Class CentralPanelOfTaskForm.
 * The CentralPanelOfTaskForm defines the view of the task form central part.
 * In addition, this class checks correctness of user answers
 * and saves information about it for estimation of testing results. 
 * @version 1.14 2015.07.23
 * @author Vitaliy Ivanytskyy
 */
public class CentralPanelOfTaskForm extends CentralPanel{
	private static final long serialVersionUID = 6725813849703857534L;
	private JPanel centralPanel;
	private JPanel topOfCentralPanel;
	private JPanel bottomOfCentralPanel;
	private JTextArea questionTextArea;
	private Task currentTaskObject;
	private int currentTaskNumber;
	//The rawResultsOfTest contains pairs <Key, Value>, where: 
	// - every key is the number of the task;
	// - every value is the result of checking answer of the task (true - correct, false - wrong)
	private HashMap<Integer, Boolean> rawResultsOfTest;
	// Correct answers for the current task in CheckBox case
	// (set "multiple" as attribute for current task in xml file)
	private HashMap<Integer, Boolean> correctAnswersForCheckBox = new HashMap<Integer, Boolean>();
	// Real answers of user for the current task in CheckBox case
	private HashMap<Integer, Boolean> userAnswersByCheckBox = new HashMap<Integer, Boolean>();
	// Message for user will show if user hasn't chosen answer yet
	// but he try to press button for load the next task 
	private JLabel messageForUser;
	// Counter of selected CheckBox elements
	int counterOfSelect = 0;
	/**
     * Constructs the centalPanel as combination of two parts, where:
     * - top part (topOfCentralPanel) is responsible for showing the text area with the question of the current task;
     * - bottom part (bottomOfCentralPanel) is responsible for showing the RadioButton
     * or CheckBox elements as variants of answer.
     */ 
	public CentralPanelOfTaskForm(Task currentTaskObject, int currentTaskNumber) {
		this.currentTaskNumber = currentTaskNumber;
		rawResultsOfTest = new HashMap<Integer, Boolean>();
		this.currentTaskObject = currentTaskObject;
		centralPanel = new JPanel(new GridLayout(2, 1, 10, 1));
		centralPanel.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createBevelBorder(BevelBorder.LOWERED),
                BorderFactory.createEmptyBorder(25, 25, 25, 25)));
		// Creating the topOfCentralPanel
		topOfCentralPanel = new JPanel();		
		String question = this.currentTaskObject.getQuestion();		
		questionTextArea = new JTextArea(5, 45);
		questionTextArea.setText(question);
		questionTextArea.setLineWrap(true);
		questionTextArea.setWrapStyleWord(true);
		questionTextArea.setEditable(false);		
		topOfCentralPanel.add(new JScrollPane(questionTextArea));
		makeBottomOfCentralPanel();
		centralPanel.add(topOfCentralPanel);
		centralPanel.add(bottomOfCentralPanel);
	}
	// The method for creating bottomOfCentralPanel
	public final void makeBottomOfCentralPanel() {
		bottomOfCentralPanel = new JPanel();
		int numberOfAnswers = currentTaskObject.getAnswers().size();
		// Operation "numberOfAnswers + 1" is reserving space for messageForUser
		bottomOfCentralPanel.setLayout(new GridLayout(numberOfAnswers + 1, 1, 10, 1));
		messageForUser = new JLabel("");
		Font fontForValueLabel = new Font(Font.DIALOG, Font.ITALIC, 12);
		messageForUser.setFont(fontForValueLabel);
		// Creating the set of radio buttons and connect a listener for each of them (RadioButton case)
		if(currentTaskObject.getAnswersType().equals("single")){
			ButtonGroup bg = new ButtonGroup();
			for (int i = 0; i < numberOfAnswers; i++) {
				final Answer currentAnswer = currentTaskObject.getAnswers().get(i);
				final JRadioButton jrb = new JRadioButton(currentAnswer.getText());
				jrb.addActionListener(new ActionListener() {					
					@Override
					public void actionPerformed(ActionEvent e) {
						messageForUser.setText("");
						if(jrb.isSelected() && currentAnswer.getMark().equals("correct")){
							rawResultsOfTest.put(currentTaskNumber, true);
						}else{
							rawResultsOfTest.put(currentTaskNumber, false);
						}						
					}
				});				
				bottomOfCentralPanel.add(jrb);
				bg.add(jrb);
			}
			bottomOfCentralPanel.add(messageForUser);
		}
		// Creating the set of check box elements and connect a listener for each of them (CheckBox case)
		else if(currentTaskObject.getAnswersType().equals("multiple")){
			makeCorrectAnswersMap(currentTaskObject);
			counterOfSelect = 0;
			for (int i = 0; i < numberOfAnswers; i++) {
				final Answer currentAnswer = currentTaskObject.getAnswers().get(i);
				final JCheckBox jcb = new JCheckBox(currentAnswer.getText());
				userAnswersByCheckBox.put(currentAnswer.getId(), false);
				jcb.addActionListener(new ActionListener() {					
					@Override
					public void actionPerformed(ActionEvent e) {
						messageForUser.setText("");
						if(jcb.isSelected() && currentAnswer.getMark().equals("correct")){
							counterOfSelect++;
							userAnswersByCheckBox.put(currentAnswer.getId(), true);
						}else if(!jcb.isSelected() && currentAnswer.getMark().equals("correct")){
							counterOfSelect--;
							userAnswersByCheckBox.put(currentAnswer.getId(), false);
						}else if(jcb.isSelected() && currentAnswer.getMark().equals("wrong")){
							userAnswersByCheckBox.put(currentAnswer.getId(), true);
							counterOfSelect++;
						}else if(!jcb.isSelected() && currentAnswer.getMark().equals("wrong")){
							userAnswersByCheckBox.put(currentAnswer.getId(), false);
							counterOfSelect--;
						}
						if(counterOfSelect != 0){
							compareAnswersInCheckBoxMaps();
						}else if(rawResultsOfTest.containsKey(currentTaskNumber) && counterOfSelect == 0){
							rawResultsOfTest.remove(currentTaskNumber);
						}						
					}
				});
				bottomOfCentralPanel.add(jcb);				
			}
			bottomOfCentralPanel.add(messageForUser);
		}else{
			JLabel label = new JLabel("incorrect data");
			bottomOfCentralPanel.add(label);
		}		
	}
	private void makeCorrectAnswersMap(Task currentTask ){
		for (Answer item : currentTask.getAnswers()) {
			if(item.getMark().equals("correct")){
				correctAnswersForCheckBox.put(item.getId(), true);
			}else{
				correctAnswersForCheckBox.put(item.getId(), false);
			}
		}	
	}
	// Comparing real answers of user  with correct answers for the current task (in CheckBox case) 
	private void compareAnswersInCheckBoxMaps(){		
		if(correctAnswersForCheckBox.entrySet().containsAll(userAnswersByCheckBox.entrySet()) 
				&& userAnswersByCheckBox.entrySet().containsAll(correctAnswersForCheckBox.entrySet())){
			rawResultsOfTest.put(currentTaskNumber, true);
		}else{
			rawResultsOfTest.put(currentTaskNumber, false);
		}
	}
	// It's the method for loading next task from controller package
	public void loadNextTask(Task currentTaskObject, int currentTaskNumber){
		this.currentTaskObject = currentTaskObject;
		this.currentTaskNumber = currentTaskNumber;
		questionTextArea.setText(this.currentTaskObject.getQuestion());
		centralPanel.remove(bottomOfCentralPanel);
		bottomOfCentralPanel.removeAll();
		makeBottomOfCentralPanel();
		centralPanel.add(bottomOfCentralPanel);
		revalidate();
	}
	@Override
	public JPanel getCentralPanel() {
		return centralPanel;
	}	
	public HashMap<Integer, Boolean> getRawResultsOfTest() {
		return rawResultsOfTest;
	}
	public JLabel getMessageForUser() {
		return messageForUser;
	}	
}