package ua.step.ivanytskyy.view;
import java.awt.GridLayout;
import javax.swing.*;
/** 
 * Class FooterPanelOfStartForm.
 * The FooterPanelOfStartForm defines the view of the start form bottom part.
 * @version 1.14 2015.07.23
 * @author Vitaliy Ivanytskyy
 */
public class FooterPanelOfStartForm extends FooterPanel{
	private static final long serialVersionUID = 8728958708223612740L;
	private JPanel footerPanel;
	private JButton buttonStart;
	private String buttonStartName = "Начать тест";
	// If user presses the buttonStart even though he didn't fill up all fields of the form
	// the message will be activated 
	private JLabel dataCompletenessMessage;
	/**
     * Constructs the footerPanel as combination of three parts, where:
     * - left part is emptyLabel;
     * - central part is responsible for showing the button for start the testing;
     * - right part is responsible for showing the message unless the user fills up every field of the start form.
     */ 
	public FooterPanelOfStartForm() {
		footerPanel = new JPanel();
		footerPanel.setLayout(new GridLayout(1, 3, 20, 5));
		JLabel emptyLabel = new JLabel("");
		footerPanel.add(emptyLabel);		
		buttonStart = new JButton(buttonStartName);
		footerPanel.add(buttonStart);
		dataCompletenessMessage = new JLabel();
		footerPanel.add(dataCompletenessMessage);		
	}
	@Override
	public JPanel getFooterPanel() {
		return footerPanel;
	}
	public JButton getButtonStart() {
		return buttonStart;
	}	
	public JLabel getDataCompletenessMessage() {
		return dataCompletenessMessage;
	}
}