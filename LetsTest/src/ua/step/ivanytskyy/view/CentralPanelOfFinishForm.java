package ua.step.ivanytskyy.view;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;
/** 
 * Class FooterPanelOfFinishForm.
 * The CentralPanelOfFinishForm defines the view of the finish form central part.
 * @version 1.14 2015.07.23
 * @author Vitaliy Ivanytskyy
 */
public class CentralPanelOfFinishForm extends CentralPanel{
	private static final long serialVersionUID = -324764018640509699L;
	private JPanel centralPanel;
	private JLabel nameLabel;
	private JLabel eMailLabel;
	private JLabel groupLabel;
	private JLabel themeLabel;
	private JLabel testLabel;
	private JLabel resultLabel;
	private JLabel testDurationLabel;
	public CentralPanelOfFinishForm() {
		centralPanel = new JPanel();
		centralPanel.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createBevelBorder(BevelBorder.LOWERED),
                BorderFactory.createEmptyBorder(25, 25, 25, 25)));
		GridBagLayout gridBagLayout = new GridBagLayout();
		GridBagConstraints  gridBagConstraints = new GridBagConstraints();
		centralPanel.setLayout(gridBagLayout);
		Font fontForValueLabel = new Font(Font.DIALOG, Font.ITALIC, 12);
		// Creating the JLabel elements
		JLabel titleNameLabel = new JLabel("ФИО: ");	
		nameLabel = new JLabel();
		nameLabel.setFont(fontForValueLabel);		
		JLabel titleEMailLabel = new JLabel("E-Mail:");		
		eMailLabel = new JLabel();
		eMailLabel.setFont(fontForValueLabel);		
		JLabel titleGroupLabel = new JLabel("Группа:");		
		groupLabel = new JLabel();
		groupLabel.setFont(fontForValueLabel);		
		JLabel titleThemeLabel = new JLabel("Тема:");		
		themeLabel = new JLabel();
		themeLabel.setFont(fontForValueLabel);		
		JLabel titleTestLabel = new JLabel("Тест:");
		testLabel = new JLabel();
		testLabel.setFont(fontForValueLabel);		
		JLabel titleResultLabel = new JLabel("Результат:");		
		resultLabel = new JLabel();
		resultLabel.setFont(fontForValueLabel);		
		JLabel titleDurationLabel = new JLabel("Время:");	
		testDurationLabel = new JLabel();
		testDurationLabel.setFont(fontForValueLabel);
		// Locating the JLabel elements in the centralPanel by using the gridBagLayout		
		gridBagConstraints.insets = new Insets(5, 15, 5, 15);
		gridBagConstraints.anchor = GridBagConstraints.WEST;		
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagLayout.setConstraints(titleNameLabel, gridBagConstraints);		
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 0;
		gridBagLayout.setConstraints(nameLabel, gridBagConstraints);		
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagLayout.setConstraints(titleEMailLabel, gridBagConstraints);		
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 1;
		gridBagLayout.setConstraints(eMailLabel, gridBagConstraints);		
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 2;
		gridBagLayout.setConstraints(titleGroupLabel, gridBagConstraints);		
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 2;
		gridBagLayout.setConstraints(groupLabel, gridBagConstraints);		
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 3;
		gridBagLayout.setConstraints(titleThemeLabel, gridBagConstraints);		
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 3;
		gridBagLayout.setConstraints(themeLabel, gridBagConstraints);		
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 4;
		gridBagLayout.setConstraints(titleTestLabel, gridBagConstraints);		
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 4;
		gridBagLayout.setConstraints(testLabel, gridBagConstraints);		
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 5;
		gridBagLayout.setConstraints(titleResultLabel, gridBagConstraints);		
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 5;
		gridBagLayout.setConstraints(resultLabel, gridBagConstraints);		
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 6;
		gridBagLayout.setConstraints(titleDurationLabel, gridBagConstraints);		
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 6;
		gridBagLayout.setConstraints(testDurationLabel, gridBagConstraints);
		centralPanel.add(titleNameLabel);
		centralPanel.add(nameLabel);
		centralPanel.add(titleEMailLabel);
		centralPanel.add(eMailLabel);
		centralPanel.add(titleGroupLabel);
		centralPanel.add(groupLabel);
		centralPanel.add(titleThemeLabel);
		centralPanel.add(themeLabel);
		centralPanel.add(titleTestLabel);
		centralPanel.add(testLabel);
		centralPanel.add(titleResultLabel);
		centralPanel.add(resultLabel);
		centralPanel.add(titleDurationLabel);
		centralPanel.add(testDurationLabel);
	}
	@Override
	public JPanel getCentralPanel() {
		return centralPanel;
	}
	public JLabel getNameLabel() {
		return nameLabel;
	}
	public JLabel geteMailLabel() {
		return eMailLabel;
	}
	public JLabel getGroupLabel() {
		return groupLabel;
	}
	public JLabel getThemeLabel() {
		return themeLabel;
	}
	public JLabel getTestLabel() {
		return testLabel;
	}
	public JLabel getResultLabel() {
		return resultLabel;
	}
	public JLabel getTestDurationLabel() {
		return testDurationLabel;
	}	
}