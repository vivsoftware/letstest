package ua.step.ivanytskyy.view;
import java.awt.BorderLayout;
import javax.swing.*;
import javax.swing.border.BevelBorder;
/** 
 * Class HeaderPanelOfTaskForm.
 * The HeaderPanelOfTaskForm defines the view of the task form top part.
 * @version 1.14 2015.07.23
 * @author Vitaliy Ivanytskyy
 */
public class HeaderPanelOfTaskForm extends HeaderPanel{
	private static final long serialVersionUID = 3331435930375483200L;
	private final int FIRST_TASK_NUMBER = 1;	
	private JPanel headerPanel;
	private JLabel leftPanelValue;
	private JLabel timeLabelInMinutes;
	private JLabel separatorLabel;
	private JLabel timeLabelInSeconds;
	private String titleForTaskNumber = "Вопрос №";
	private String titleForRemainingTime = "Оставшееся время: ";
	private String timeIndicatorsSeparator = ":";
	/**
	 * Constructs the headerPanel as combination of two parts, where:
	 * - left part is responsible for showing the number of the current task;
	 * - right part is responsible for showing the remaining time of the testing process.
	 */ 
	public HeaderPanelOfTaskForm() {
		headerPanel = new JPanel();
		headerPanel.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createBevelBorder(BevelBorder.RAISED),
                BorderFactory.createEmptyBorder(5, 5, 5, 5)));
		headerPanel.setLayout(new BorderLayout());
		// Creating the left part of panel and adding the number of the first task
		JPanel leftPanel = new JPanel();
		JLabel leftPanelTitle = new JLabel(titleForTaskNumber);
		leftPanelValue = new JLabel(String.valueOf(FIRST_TASK_NUMBER));
		leftPanel.add(leftPanelTitle);
		leftPanel.add(leftPanelValue);	
		// Creating the right part of panel and adding the initial values
		// for displaying the remaining time of the testing process
		JPanel rightPanel = new JPanel();
		JLabel rightPanelTitle = new JLabel(titleForRemainingTime);
		timeLabelInMinutes = new JLabel("00");
		separatorLabel = new JLabel(timeIndicatorsSeparator);
		timeLabelInSeconds = new JLabel("00");
		rightPanel.add(rightPanelTitle);
		rightPanel.add(timeLabelInMinutes);
		rightPanel.add(separatorLabel);
		rightPanel.add(timeLabelInSeconds);
		// Add left and right parts into the headerPanel
		headerPanel.add(leftPanel, BorderLayout.WEST);
		headerPanel.add(rightPanel, BorderLayout.EAST);
	}
	@Override
	public JPanel getHeaderPanel() {
		return headerPanel;
	}
	public JLabel getTimeLabelInMinutes() {
		return timeLabelInMinutes;
	}
	public JLabel getSeparatorLabel() {
		return separatorLabel;
	}
	public JLabel getTimeLabelInSeconds() {
		return timeLabelInSeconds;
	}
	public JLabel getLeftPanelValue() {
		return leftPanelValue;
	}
}