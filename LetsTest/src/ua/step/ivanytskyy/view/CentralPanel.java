package ua.step.ivanytskyy.view;
import javax.swing.*;
/** 
 * Abstract class CentralPanel.
 * The abstract class CentralPanel defines the common interface for his subclasses.
 * @version 1.14 2015.07.23
 * @author Vitaliy Ivanytskyy
 */
public abstract class CentralPanel extends JPanel{
	private static final long serialVersionUID = 596321034180723828L;
	public abstract JPanel getCentralPanel();	
}