package ua.step.ivanytskyy.view;
import javax.swing.*;
/** 
 * Abstract class HeaderPanel.
 * The abstract class HeaderPanel defines the common interface for his subclasses.
 * @version 1.14 2015.07.23
 * @author Vitaliy Ivanytskyy
 */
public abstract class HeaderPanel extends JPanel{
	private static final long serialVersionUID = 3331435930375483200L;
	public abstract JPanel getHeaderPanel();
}