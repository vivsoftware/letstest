package ua.step.ivanytskyy.view;
import java.awt.Dimension;
import javax.swing.JButton;
import javax.swing.JPanel;
/** 
 * Class FooterPanelOfFinishForm.
 * The FooterPanelOfFinishForm defines the view of the finish form bottom part.
 * @version 1.14 2015.07.23
 * @author Vitaliy Ivanytskyy
 */
public class FooterPanelOfFinishForm extends FooterPanel{
	private static final long serialVersionUID = -7535515552477904103L;
	private JPanel footerPanel;		
	private JButton buttonNewTest;
	private JButton buttonExit;
	private String buttonNewTestName = "Новый тест";
	private String buttonExitName = "Выход из программы";
	public FooterPanelOfFinishForm() {
		footerPanel = new JPanel();
		Dimension preferredSize = new Dimension(160, 26);
		buttonNewTest = new JButton(buttonNewTestName);		
		buttonNewTest.setPreferredSize(preferredSize);
		buttonExit = new JButton(buttonExitName);
		buttonExit.setPreferredSize(preferredSize);
		footerPanel.add(buttonNewTest);
		footerPanel.add(buttonExit);
	}
	@Override
	public JPanel getFooterPanel() {
		return footerPanel;
	}
	public JButton getButtonExit() {
		return buttonExit;
	}
	public JButton getButtonNewTest() {
		return buttonNewTest;
	}
}