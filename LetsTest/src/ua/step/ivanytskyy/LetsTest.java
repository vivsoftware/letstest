/**
 * Let's Test project.
 * The goal of this project is developing software for knowledge testing in any field.   
 */
package ua.step.ivanytskyy;
import java.awt.EventQueue;
import ua.step.ivanytskyy.view.*;
/** 
 * Class LetsTest
 * Class LetsTest (with main method) is entry point of program Let's Test.
 * This class methods was developed for creating the frame in the dispatch thread
 * of the system EventQueue
 * @version 1.14 2015.07.23
 * @author Vitaliy Ivanytskyy
 */
public class LetsTest {
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable(){
			@Override
            public void run(){
                try{
                    createFrame();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });	
	}
	// Method for creating the frame with objects for the start form. 
	public static void createFrame(){
		ViewForm frame = new ViewForm(
				"Программа тестирования знаний Let's Test",   // title of start form
				new HeaderPanelOfStartForm(),
				new CentralPanelOfStartForm(),
				new FooterPanelOfStartForm(),
				600, 400                                      // width and height of frame
				);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
	}
}