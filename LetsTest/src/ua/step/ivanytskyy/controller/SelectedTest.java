package ua.step.ivanytskyy.controller;
import java.io.File;
import java.util.ArrayList;
import java.util.Random;
import ua.step.ivanytskyy.model.Task;
import ua.step.ivanytskyy.model.Test;
import ua.step.ivanytskyy.model.TestDeterminator;
/** 
 * Class SelectedTest
 * Class SelectedTest was developed for preparing the final list of tasks
 * and getting properties of the current test.
 * @version 1.14 2015.07.23
 * @author Vitaliy Ivanytskyy
 */
public class SelectedTest {
	// The current test object
	private Test selectedTest;
	private int numberOfTasks;
	private int durationOfTest;
	// The final list of tasks for testing
	private ArrayList<Task> listOfTasks;
	/**
     * Constructs a list of tasks.
     * Number of tasks depend from the number was set by a teacher
     * into the numberOfTasks xml element (see the xml file with tasks).
     * @param  selectedFile
     *         The File object (for the xml file) with tests
     * @param  testName
     *         The name of selected test
     */
	public SelectedTest(File selectedFile, String testName){
		// Creating the object of the TestDeterminator class for getting test with his name
		TestDeterminator testDeterminator = new TestDeterminator(selectedFile, testName);
		selectedTest = testDeterminator.getCurrentTestObj();
		// Getting the predetermined number of tasks for current test
		numberOfTasks = selectedTest.getNumberOfTasks();
		durationOfTest = selectedTest.getTestTime();
		listOfTasks = new ArrayList<Task>();
		// Getting the full list of the tasks
		ArrayList<Task> listOfAllTasks = selectedTest.getTasks();
		// Making the final list of tasks for testing
		if(listOfAllTasks.size() > numberOfTasks){
			randomizeListOfTasks(listOfAllTasks);
		}else{
			listOfTasks = listOfAllTasks;			
			numberOfTasks = listOfTasks.size();
		}
	}
	// Method for randomizing tests
	private void randomizeListOfTasks(ArrayList<Task> listOfAllTasks){
		Random randomNumber = new Random();		
		for (int i = 0; i < numberOfTasks; i++) {
			int index = randomNumber.nextInt(listOfAllTasks.size());
			listOfTasks.add(listOfAllTasks.get(index));
			listOfAllTasks.remove(index);
		}
	}
	public Test getSelectedTest() {
		return selectedTest;
	}
	public int getNumberOfTasks() {
		return numberOfTasks;
	}
	public int getDurationOfTest() {
		return durationOfTest;
	}
	public ArrayList<Task> getListOfTasks() {
		return listOfTasks;
	}	
}