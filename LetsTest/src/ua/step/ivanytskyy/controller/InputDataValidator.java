package ua.step.ivanytskyy.controller;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/** 
 * Class InputDataValidator.
 * Class InputDataValidator was developed for validation data,
 * which will be inputed by user into the fields of start form.
 * @version 1.14 2015.07.23
 * @author Vitaliy Ivanytskyy
 */
public class InputDataValidator {
	// Method for checking the e-mail on conformity with our requirements by using the regular expression
	public static boolean checkEMail(String eMail){
		String regexOfEMail = "[a-zA-Z0-9\\-_'+]+([\\.])?[a-zA-Z0-9\\-_'+]+@[a-z0-9]+([\\.])?[a-z0-9]+\\.([a-z]){2,6}";
		Pattern patternOfEMail = Pattern.compile(regexOfEMail);			
		Matcher matcherOfEMail = patternOfEMail.matcher(eMail);
		boolean checkingResult = matcherOfEMail.matches();			
		return (checkingResult && checkLengthString(eMail, 60)) ? true : false;
	}
	// Method for checking the user name on conformity with our requirements by using the regular expression
	public static boolean checkName(String name){
		String regexOfName = "[A-Za-zА-Яа-я'\\s]{2,60}";
		Pattern patternOfName = Pattern.compile(regexOfName);			
		Matcher matcherOfName = patternOfName.matcher(name);
		boolean checkingResult = matcherOfName.matches();			
		return (checkingResult) ? true : false;
	}
	// Method for checking the user group number (in string representation)
	// on conformity with our requirements by using the regular expression
	public static boolean checkGroupNumber(String groupNumber){
		String regexOfGroupNumber = "[A-Za-zА-Яа-я\\s-#№0-9]{2,60}";
		Pattern patternOfGroupNumber = Pattern.compile(regexOfGroupNumber);			
		Matcher matcherOfGroupNumber = patternOfGroupNumber.matcher(groupNumber);
		boolean checkingResult = matcherOfGroupNumber.matches();			
		return (checkingResult) ? true : false;
	}
	// Method for checking the length of the string on conformity with our requirements
	public static boolean checkLengthString(String string, int requiredLength){
		return (string.length() <= requiredLength) ? true : false;
	}
}