package ua.step.ivanytskyy.controller;
import java.util.ArrayList;
import java.util.HashMap;
import ua.step.ivanytskyy.model.Task;
/** 
 * Class ResultEstimation
 * Class ResultEstimation was developed for estimating the result of the test.
 * This class prepares the output data for filling up the finish form
 * and the xml file with test results.
 * @version 1.14 2015.07.23
 * @author Vitaliy Ivanytskyy
 */
public class ResultEstimation {
	// The listOfTasks is storage for all the tasks of the current test 
	private ArrayList<Task> listOfTasks;
	/*
	 * The rawResultsOfTest field is HashMap<Integer, Boolean>, where:
	 * - every key is the number of the task;
	 * - every value is the result of checking answer of the task (true - correct, false - wrong) 
	 */
	private HashMap<Integer, Boolean> rawResultsOfTest;
	// Tasks total number of current test
	private int numberOfTasks;
	private int numberOfCorrectAnswer;
	private int numberOfWrongAnswer;
	private int numberOfMissedQuestion;
	private int grade;
	private String resultsOfTest;
	public ResultEstimation(ArrayList<Task> listOfTasks, HashMap<Integer, Boolean> rawResultsOfTest){		
		this.listOfTasks = listOfTasks;
		this.rawResultsOfTest = rawResultsOfTest;
		numberOfTasks = this.listOfTasks.size();
		numberOfCorrectAnswer = 0;
		numberOfWrongAnswer = 0;
		// Counting number of the correct and wrong answers 
		for (Integer key : this.rawResultsOfTest.keySet()) {
			if(rawResultsOfTest.get(key) != null && this.rawResultsOfTest.get(key) == true){
				numberOfCorrectAnswer++;
			}else{
				numberOfWrongAnswer++;
			}
		}
		// Counting number of the questions was missed because test time was finished   
		numberOfMissedQuestion = this.listOfTasks.size() - (numberOfCorrectAnswer + numberOfWrongAnswer);
		// Counting grade as ratio the correct answers number to the tasks total number of current test (%) 
		grade = numberOfCorrectAnswer * 100 / numberOfTasks;
		// Preparing the report string about test result for the finish form
		resultsOfTest = numberOfCorrectAnswer + " правильных ответа из " + listOfTasks.size()
				+ " (" + grade + "%)";
	}
	public String getResultsOfTest() {
		return resultsOfTest;
	}
	public int getNumberOfTasks(){
		return numberOfTasks;
	}
	public int getNumberOfCorrectAnswer() {
		return numberOfCorrectAnswer;
	}
	public int getNumberOfWrongAnswer() {
		return numberOfWrongAnswer;
	}
	public int getNumberOfMissedQuestion() {
		return numberOfMissedQuestion;
	}
	public int getGrade(){
		return grade;
	}	
}