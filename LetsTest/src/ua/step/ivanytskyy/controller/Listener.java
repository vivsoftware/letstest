package ua.step.ivanytskyy.controller;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.Timer;
import ua.step.ivanytskyy.model.*;
import ua.step.ivanytskyy.view.*;
/** 
 * Class Listener
 * The class Listener is central class in ua.step.ivanytskyy.controller package of Let's Test program.
 * This class was developed for:
 * - listening elements of view classes;
 * - management of the application logic;
 * - ensuring interaction between objects of view classes and objects of model classes 
 * @version 1.14 2015.07.23
 * @author Vitaliy Ivanytskyy
 */
public class Listener {
	private Student currentStudent;
	private ViewForm viewFormObject;
	private Timer timer;
	private SelectedTest selectedTest;
	private ArrayList<Task> listOfTasks;
	private TestDeterminator testDeterminator;
	private File currentFile;
	private int counterOfTasks;
	private int numberOfCurrentTask;
	private ArrayList<Integer> postponedTasksNumbers = new ArrayList<Integer>();
	private TimerTick timerTick;
	private HeaderPanelOfTaskForm headerPanelOfTaskForm;
	private CentralPanelOfTaskForm centralPanelOfTaskForm;
	private FooterPanelOfTaskForm footerPanelOfTaskForm;
	private HeaderPanelOfFinishForm headerPanelOfFinishForm;
	private CentralPanelOfFinishForm centralPanelOfFinishForm;
	private FooterPanelOfFinishForm footerPanelOfFinishForm;	
	private String messageForUser = "Ответьте на вопрос или отложите его!";
	/** 
	* Class constructor.
	* New Student object is being created with this constructor. 
	* @param viewFormObject is object of ua.step.ivanytskyy.view.ViewForm class	* 
	*/
	public Listener(ViewForm viewFormObject){
		currentStudent = new Student();
		this.viewFormObject = viewFormObject;
	}
	// Identification the objects of CentralPanel subclasses and calling the methods for listening components of them 
	public void listenCentralPanel(final CentralPanel object) {
		if(object.getClass().getName().equals("ua.step.ivanytskyy.view.CentralPanelOfStartForm")){
			final CentralPanelOfStartForm objectOfCentralPanelOfStartForm = (CentralPanelOfStartForm) object;		
			listenCentralPanelOfStartForm(objectOfCentralPanelOfStartForm);
		}
	}
	// Identification the objects of FooterPanel subclasses and calling the methods for listening components of them
	public void listenFooterPanel(final FooterPanel object) {
		if(object.getClass().getName().equals("ua.step.ivanytskyy.view.FooterPanelOfStartForm")){
			final FooterPanelOfStartForm objectOfFooterPanelOfStartForm = (FooterPanelOfStartForm) object;		
			listenFooterPanelOfStartForm(objectOfFooterPanelOfStartForm);
		}
		if(object.getClass().getName().equals("ua.step.ivanytskyy.view.FooterPanelOfTaskForm")){
			final FooterPanelOfTaskForm objectOfFooterPanelOfTaskForm = (FooterPanelOfTaskForm) object;		
			listenFooterPanelOfTaskForm(objectOfFooterPanelOfTaskForm);
		}
		if(object.getClass().getName().equals("ua.step.ivanytskyy.view.FooterPanelOfFinishForm")){
			final FooterPanelOfFinishForm objectOfFooterPanelOfFinishForm = (FooterPanelOfFinishForm) object;		
			listenFooterPanelOfFinishForm(objectOfFooterPanelOfFinishForm);
		}		
	}
	// Listening the components of CentralPanelOfStartForm objects
	public void listenCentralPanelOfStartForm(final CentralPanelOfStartForm object){
		object.getNameTextField().addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				String userName = object.getNameTextField().getText().trim();				
				if (InputDataValidator.checkName(userName)) {
					object.getNameTextFieldChecker().setText("корректно");
					object.getNameTextFieldChecker().setForeground(Color.GREEN);
					currentStudent.setName(userName);
				} else if (userName.isEmpty()) {
					object.getNameTextFieldChecker().setText("пусто");
					object.getNameTextFieldChecker().setForeground(Color.RED);
					currentStudent.setName(null);
				} else {
					object.getNameTextFieldChecker().setText("не корректно");
					object.getNameTextFieldChecker().setForeground(Color.RED);
					currentStudent.setName(null);
				}
			}
		});
		object.getNameTextField().addFocusListener(new FocusListener() {			
			@Override
			public void focusLost(FocusEvent e) {
				String userName = object.getNameTextField().getText().trim();				
				if (InputDataValidator.checkName(userName)) {
					object.getNameTextFieldChecker().setText("корректно");
					object.getNameTextFieldChecker().setForeground(Color.GREEN);
					currentStudent.setName(userName);
				} else if (userName.isEmpty()) {
					object.getNameTextFieldChecker().setText("пусто");
					object.getNameTextFieldChecker().setForeground(Color.RED);
					currentStudent.setName(null);
				} else {
					object.getNameTextFieldChecker().setText("не корректно");
					object.getNameTextFieldChecker().setForeground(Color.RED);
					currentStudent.setName(null);
				}				
			}			
			@Override
			public void focusGained(FocusEvent e) {
				// TODO Auto-generated method stub				
			}
		});
		object.getEmailTextField().addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				String userEMail = object.getEmailTextField().getText().trim();
				if (InputDataValidator.checkEMail(userEMail)) {
					object.getEMailTextFieldChecker().setText("корректно");
					object.getEMailTextFieldChecker().setForeground(Color.GREEN);
					currentStudent.setEmail(userEMail);
				} else if (userEMail.isEmpty()) {
					object.getEMailTextFieldChecker().setText("пусто");
					object.getEMailTextFieldChecker().setForeground(Color.RED);
					currentStudent.setEmail(null);
				} else {
					object.getEMailTextFieldChecker().setText("не корректно");
					object.getEMailTextFieldChecker().setForeground(Color.RED);
					currentStudent.setEmail(null);
				}
			}
		});
		object.getEmailTextField().addFocusListener(new FocusListener() {			
			@Override
			public void focusLost(FocusEvent e) {
				String userEMail = object.getEmailTextField().getText().trim();
				if (InputDataValidator.checkEMail(userEMail)) {
					object.getEMailTextFieldChecker().setText("корректно");
					object.getEMailTextFieldChecker().setForeground(Color.GREEN);
					currentStudent.setEmail(userEMail);
				} else if (userEMail.isEmpty()) {
					object.getEMailTextFieldChecker().setText("пусто");
					object.getEMailTextFieldChecker().setForeground(Color.RED);
					currentStudent.setEmail(null);
				} else {
					object.getEMailTextFieldChecker().setText("не корректно");
					object.getEMailTextFieldChecker().setForeground(Color.RED);
					currentStudent.setEmail(null);
				}		
			}			
			@Override
			public void focusGained(FocusEvent e) {
				// TODO Auto-generated method stub				
			}
		});
		object.getGroupTextField().addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				String userGroup = object.getGroupTextField().getText().trim();
				if (InputDataValidator.checkGroupNumber(userGroup)) {
					object.getGroupTextFieldChecker().setText("корректно");
					object.getGroupTextFieldChecker().setForeground(Color.GREEN);
					currentStudent.setGroup(userGroup);					
				} else if (userGroup.isEmpty()) {
					object.getGroupTextFieldChecker().setText("пусто");
					object.getGroupTextFieldChecker().setForeground(Color.RED);
					currentStudent.setGroup(null);
				} else {
					object.getGroupTextFieldChecker().setText("не корректно");
					object.getGroupTextFieldChecker().setForeground(Color.RED);
					currentStudent.setGroup(null);
				}
			}
		});
		object.getGroupTextField().addFocusListener(new FocusListener() {			
			@Override
			public void focusLost(FocusEvent e) {
				String userGroup = object.getGroupTextField().getText().trim();
				if (InputDataValidator.checkGroupNumber(userGroup)) {
					object.getGroupTextFieldChecker().setText("корректно");
					object.getGroupTextFieldChecker().setForeground(Color.GREEN);
					currentStudent.setGroup(userGroup);					
				} else if (userGroup.isEmpty()) {
					object.getGroupTextFieldChecker().setText("пусто");
					object.getGroupTextFieldChecker().setForeground(Color.RED);
					currentStudent.setGroup(null);
				} else {
					object.getGroupTextFieldChecker().setText("не корректно");
					object.getGroupTextFieldChecker().setForeground(Color.RED);
					currentStudent.setGroup(null);
				}	
			}			
			@Override
			public void focusGained(FocusEvent e) {
				// TODO Auto-generated method stub				
			}
		});
		object.getThemesComboBox().addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				String theme = object.getThemesComboBox().getSelectedItem().toString();
				XmlFileSearcher xmlFileSearcher = new XmlFileSearcher("Tests\\");
				for (int i = 0; i < xmlFileSearcher.getFilesArray().length; i++) {
					if(xmlFileSearcher.getFileNamesArray()[i].equals(theme)){
						object.getThemesComboBoxChecker().setText("корректно");
						object.getThemesComboBoxChecker().setForeground(Color.GREEN);
						currentFile =  xmlFileSearcher.getFilesArray()[i];
						testDeterminator = new TestDeterminator(currentFile);
						object.setTestsList(testDeterminator.getTestsNameArray());
						currentStudent.setTheme(theme);
						currentStudent.setDate(new Date().toString());
					}
				}
			}
		});		
		object.getTestsComboBox().addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(object.getTestsComboBox().getSelectedItem() != null){
					String testName = object.getTestsComboBox().getSelectedItem().toString();
					selectedTest = new SelectedTest(currentFile, testName);
					object.setTasksNumber(selectedTest.getNumberOfTasks() + "");
					object.setTestTime(selectedTest.getDurationOfTest() + " мин.");
					currentStudent.setTest(testName);
				}
			}
		});		
	}
	// Listening the components of FooterPanelOfStartForm objects
	public void listenFooterPanelOfStartForm(final FooterPanelOfStartForm object){
		object.getButtonStart().addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				// Checking if all fields were filled up
				if(currentStudent.getName() != null 						
						&& currentStudent.getEmail() != null 
						&& currentStudent.getGroup() != null 
						&& currentStudent.getTheme() != null 
						&& currentStudent.getTest() != null){
					object.getDataCompletenessMessage().setText(null);
					listOfTasks = selectedTest.getListOfTasks();
					counterOfTasks = 0;
					numberOfCurrentTask = 0;
					headerPanelOfTaskForm= new HeaderPanelOfTaskForm();
					centralPanelOfTaskForm = new CentralPanelOfTaskForm(listOfTasks.get(counterOfTasks), counterOfTasks);
					footerPanelOfTaskForm = new FooterPanelOfTaskForm();
					// Renew the frame
					viewFormObject.createViewForm(
							"Программа тестирования знаний Let's Test",   // title of task form
							headerPanelOfTaskForm,
							centralPanelOfTaskForm,
							footerPanelOfTaskForm,
							600, 400);                                    // width and height of frame
					// Creating and starting the timer
					timerTick = new TimerTick();
					timer = new Timer(1000, timerTick);
					timer.start();
				}
			}
		});
		object.getButtonStart().addFocusListener(new FocusListener() {			
			@Override
			public void focusLost(FocusEvent e) {
				object.getDataCompletenessMessage().setText("");
			}			
			@Override
			public void focusGained(FocusEvent e) {
				// Checking if all fields were filled up
				if(currentStudent.getName() == null 						
						|| currentStudent.getEmail() == null 
						|| currentStudent.getGroup() == null 
						|| currentStudent.getTheme() == null 
						|| currentStudent.getTest() == null){
					// If user presses the buttonStart even though he didn't fill up all fields of the form
					// the message will be activated
					object.getDataCompletenessMessage().setText("Введите все данные!");
					object.getDataCompletenessMessage().setForeground(Color.RED);
				}			
			}
		});
	}
	// Listening the components of FooterPanelOfTaskForm objects
	public void listenFooterPanelOfTaskForm(final FooterPanelOfTaskForm object){
		object.getButtonNext().addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(centralPanelOfTaskForm.getRawResultsOfTest().containsKey(numberOfCurrentTask)){					
					if(counterOfTasks < listOfTasks.size() - 1){
						numberOfCurrentTask = ++counterOfTasks;
						headerPanelOfTaskForm.getLeftPanelValue().setText((counterOfTasks + 1) + "");
						centralPanelOfTaskForm.loadNextTask(listOfTasks.get(counterOfTasks), counterOfTasks);						
					}else if(counterOfTasks >= listOfTasks.size() - 1){
						if(postponedTasksNumbers.size() == 0){
							timer.stop();
							makeReport();
						}
						// Loading the tasks which was postponed
						else{							
							numberOfCurrentTask  = postponedTasksNumbers.get(0);
							headerPanelOfTaskForm.getLeftPanelValue().setText((numberOfCurrentTask + 1) + "");
							centralPanelOfTaskForm.loadNextTask(listOfTasks.get(numberOfCurrentTask), numberOfCurrentTask);					
							postponedTasksNumbers.remove(0);
						}
					}
				}else{
					if(counterOfTasks == listOfTasks.size() - 1){
						// If there are only questions which was postponed
						messageForUser = "Ответьте на вопрос!";
					}
					// If user presses the buttonNext even though he didn't choose the answer
					// the message will be activated
					centralPanelOfTaskForm.getMessageForUser().setText(messageForUser);
					centralPanelOfTaskForm.getMessageForUser().setForeground(Color.RED);
				}
			}
		});
		object.getButtonPostpone().addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {				
				if(counterOfTasks < listOfTasks.size() - 1){
					centralPanelOfTaskForm.getRawResultsOfTest().remove(counterOfTasks);
					postponedTasksNumbers.add(counterOfTasks);
					numberOfCurrentTask = ++counterOfTasks;
					headerPanelOfTaskForm.getLeftPanelValue().setText((counterOfTasks + 1) + "");
					centralPanelOfTaskForm.loadNextTask(listOfTasks.get(counterOfTasks), counterOfTasks);
				}else if(counterOfTasks >= listOfTasks.size() - 1){
					// If there are only questions which was postponed
					footerPanelOfTaskForm.getButtonPostpone().setEnabled(false);
					messageForUser = "Ответьте на вопрос!";
				}
			}
		});		
	}
	// Listening the components of FooterPanelOfFinishForm objects
	public void listenFooterPanelOfFinishForm(final FooterPanelOfFinishForm object){
		object.getButtonExit().addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);				
			}
		});
		object.getButtonNewTest().addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				currentStudent = new Student();
				messageForUser = "Ответьте на вопрос или отложите его!";
				viewFormObject.createViewForm(
						"Программа тестирования знаний Let's Test",   // title of start form
						new HeaderPanelOfStartForm(),
						new CentralPanelOfStartForm(),
						new FooterPanelOfStartForm(),
						600, 400);                                    // width and height of frame
			}
		});
	}
	// It's method for representation the real test time as a string 
	private String countDuration(){
		int fullDurationOfTest = selectedTest.getDurationOfTest() * 60;
		int remainedTime = timerTick.getRemainedTimeCounter();
		int realTestTime = (fullDurationOfTest - remainedTime);
		int realTestTimeInSeconds = realTestTime % 60;
		int realTestTimeInMinutes = realTestTime / 60;
		String realTestTimeAsString = (((realTestTimeInMinutes == 0) ? "" : (realTestTimeInMinutes + " мин. "))
				+ ((realTestTimeInSeconds == 0) ? "" : (realTestTimeInSeconds + " сек. ")));
		return realTestTimeAsString;
	}
	// It's method for making the report of current test and loading the finish form
	private void makeReport(){
		ResultEstimation resultEstimation = new ResultEstimation(listOfTasks,
				centralPanelOfTaskForm.getRawResultsOfTest());						
		currentStudent.setNumberOfQuestions(resultEstimation.getNumberOfTasks());
		currentStudent.setCorrectAnswersNumber(resultEstimation.getNumberOfCorrectAnswer());
		currentStudent.setWrongAnswersNumber(resultEstimation.getNumberOfWrongAnswer());
		currentStudent.setMissedQuestionsNumber(resultEstimation.getNumberOfMissedQuestion());
		currentStudent.setTestTime(countDuration());
		currentStudent.setGrade(resultEstimation.getGrade());
		headerPanelOfFinishForm = new HeaderPanelOfFinishForm();
		centralPanelOfFinishForm = new CentralPanelOfFinishForm();
		footerPanelOfFinishForm = new FooterPanelOfFinishForm();
        viewFormObject.createViewForm(
        		"Программа тестирования знаний Let's Test",   // title of finish form
        		headerPanelOfFinishForm,
        		centralPanelOfFinishForm,
        		footerPanelOfFinishForm,
        		600, 400);                                    // width and height of frame
        centralPanelOfFinishForm.getNameLabel().setText(currentStudent.getName());
        centralPanelOfFinishForm.geteMailLabel().setText(currentStudent.getEmail());
        centralPanelOfFinishForm.getGroupLabel().setText(currentStudent.getGroup());
        centralPanelOfFinishForm.getThemeLabel().setText(currentStudent.getTheme());
        centralPanelOfFinishForm.getTestLabel().setText(currentStudent.getTest());
        centralPanelOfFinishForm.getResultLabel().setText(resultEstimation.getResultsOfTest());			            
        centralPanelOfFinishForm.getTestDurationLabel().setText(currentStudent.getTestTime());        
        // Writing the results of current user testing
        try {
			new XmlWriter(currentStudent);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	/** 
	 * Class TimerTick
	 * The class TimerTick is private inner class of the Listener class.
	 * This class was developed for counting and representation remained time
	 * of the current test in the headerPanelOfTaskForm.
	 * @version 1.14 2015.07.23
	 * @author Vitaliy Ivanytskyy
	 */
	private class TimerTick implements ActionListener {
		private int remainedTimeCounter = selectedTest.getDurationOfTest() * 60;
		@Override
		public void actionPerformed(ActionEvent e) {
			remainedTimeCounter--;
			if(remainedTimeCounter / 60 > 9){
				headerPanelOfTaskForm.getTimeLabelInMinutes().setText(String.valueOf(remainedTimeCounter / 60));
			}else if(remainedTimeCounter / 60 > 0){
				headerPanelOfTaskForm.getTimeLabelInMinutes().setText("0" + String.valueOf(remainedTimeCounter / 60));
			}else{
				headerPanelOfTaskForm.getTimeLabelInMinutes().setText("00");
			}
			if(remainedTimeCounter % 60 > 9){
				headerPanelOfTaskForm.getTimeLabelInSeconds().setText(String.valueOf(remainedTimeCounter % 60));
			}else if(remainedTimeCounter % 60 > 0){
				headerPanelOfTaskForm.getTimeLabelInSeconds().setText("0" + String.valueOf(remainedTimeCounter % 60));
			}else{
				headerPanelOfTaskForm.getTimeLabelInSeconds().setText("00");
			}
			// If the time has expired, the test is finished
			if (remainedTimeCounter == 0) {
				timer.stop();
				makeReport();
			}
		}
		public int getRemainedTimeCounter() {
			return remainedTimeCounter;
		}
	}
}